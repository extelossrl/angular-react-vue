import React from 'react';
import PropTypes from 'prop-types';
import { useState } from 'react';
import './panel.css';

function Panel (props) {
  const [opened, setOpened] = useState(true);

  const toggleOpened = () => {
    setOpened(!opened)
  };

  const iconClickHandler = (event) => {
    event.stopPropagation();
    props.iconClick(props.title);
  };

  return (<div className="panel">
    <div className="title" onClick={toggleOpened} >
      {props.title}

      {props.title ? <span className="icon" onClick={event => iconClickHandler(event)}>
        {props.icon}
      </span> : null}

    </div>
    {opened ? <div>
      {props.children}
    </div> : null}
  </div>)
}

export default Panel;


Panel.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.node,
  iconClick: PropTypes.func,
};