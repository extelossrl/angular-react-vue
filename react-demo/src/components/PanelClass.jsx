import React from 'react';
import PropTypes from "prop-types";
import './panel.css';

 export default class PanelClass extends React.Component {
  state = { opened: true };

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(prevProps, this.props)
  }

   toggleOpened = () => {
    this.setState(state => ({ opened: !state.opened }))
  };

  iconClickHandler = (event) => {
    event.stopPropagation();
    this.props.iconClick(this.props.title);
  };

  render() {
    return (<div className="panel">
      <div className="title" onClick={this.toggleOpened} >
        {this.props.title}

        {this.props.title ? <span className="icon" onClick={event => this.iconClickHandler(event)}>
          {this.props.icon}
        </span> : null}
      </div>
      {this.state.opened ? <div>
        {this.props.children}
      </div> : null}
    </div>)
  }

}

PanelClass.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.node,
  iconClick: PropTypes.func,
};