import React, { Component } from 'react';
import Panel from './components/Panel'
import PanelClass from './components/PanelClass'
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { user: 'Little Tony' }
    /*
    setTimeout(() => {
      this.setState({ user: 'Gino Paoli'})
    }, 3000);
    */
  }
  addToFavorite = (title) => {
    console.log('addToFavorite', title);
  };

  render() {
    return (
      <div className="App">
        <img  src={logo} className="logo" alt=""/>
        <Panel title="Mario Rossi" icon="&#10029;" iconClick={this.addToFavorite}>
          <input type="text"/>
          <input type="text"/>
          <input type="text"/>
          <button>Go</button>
        </Panel>

        <PanelClass title={this.state.user} icon="&#10029;" iconClick={this.addToFavorite}>
          Something else
        </PanelClass>

        <PanelClass>
          Something else
        </PanelClass>
      </div>
    );
  }
}

export default App;
