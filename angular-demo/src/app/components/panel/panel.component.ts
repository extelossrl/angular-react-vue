import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnChanges {
  @Input() title: string;
  @Input() icon: string;
  @Output() iconClick: EventEmitter<string> = new EventEmitter();
  opened = true;

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

  iconClickHandler(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit(this.title);
  }
}
