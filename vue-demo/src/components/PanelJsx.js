import './panel.css';
import {map as iconsMap} from "../assets/icons"
export default {
    name: "PanelJsx",
    props: {
        title: {
            type: String,
            default: ""
        },
        icon: {
            type: String,
            default: ""
        }
    },
    data() {
        return {
            opened: true
        };
    },
    computed: {
        iconCode() {
            return iconsMap[this.icon];
        }
    },
    methods: {
        iconClickHandler(event) {
            event.stopPropagation();
            this.$emit("iconClick", this.title);
        },
        toggleOpened() {
            this.opened = !this.opened;
        }
    },
    watch: {
        title: {
            handler(value) {
                console.log(`This awesome title ${value}!`);
            },
            immediate: true
        }
    },
    render() {
        return (<div class="panel">
            <div class="title" onClick={this.toggleOpened} >
                {this.title}
                {this.title ?
                    <span
                        class="icon"
                        domPropsInnerHTML={this.iconCode}
                        onClick={$event => {this.iconClickHandler($event)}}/>
                    : null}

            </div>
            {this.opened ?
                <div>
                    {this.$slots.default}
                </div>
                : null}
        </div>)
    }

}
